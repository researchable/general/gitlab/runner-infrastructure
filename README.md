# SDV Automatic Infrastructure

Todo:
- [x] Test if this works
- [x] Enable syncing of state to e.g. digital ocean space (see https://medium.com/@timhberry/terraform-pipelines-in-jenkins-47267129ff06)
- [x] Initialize the access to the docker repository (untested)
- [x] load balancer
- [ ] Set up the domainnames


- Destroy all droplets:
`doctl compute droplet list | awk '{print $2}' | sed '1d' | xargs doctl compute droplet delete -f`



