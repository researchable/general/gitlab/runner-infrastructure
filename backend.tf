terraform {
    backend "s3" {
        skip_credentials_validation = true
        skip_metadata_api_check = true
        endpoint = "https://ams3.digitaloceanspaces.com"
        region = "us-east-1" // Not used
        bucket = "state" // name of your space
        key = "runners.tfstate" // Name of the file
    }
}
