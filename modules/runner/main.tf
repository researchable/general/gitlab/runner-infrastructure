# The SSH key of Gitlab DO is 26125739

resource "digitalocean_droplet" "runner" {
  image  = "docker-20-04"
  name   = var.runner_name
  region = var.do_region
  size   = var.do_node
  ssh_keys = [26125739, 26083592]
}

resource "null_resource" "runner-exec" {
  triggers = {
    key = "key-${uuid()}"
  }

  provisioner "remote-exec" {
    connection {
      host = digitalocean_droplet.runner.ipv4_address
      type = "ssh"
      user = "root"
      private_key = var.ssh_private_key
    }
    inline = [
      "curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | bash",
      "export GITLAB_RUNNER_DISABLE_SKEL=true; apt-get update",
      "export GITLAB_RUNNER_DISABLE_SKEL=true; apt-get install -y gitlab-runner",
      "gitlab-runner unregister --all-runners",
      "gitlab-runner register --non-interactive --docker-pull-policy 'if-not-present' --url 'https://gitlab.com/' --registration-token '${var.project_registration_token}' --executor 'docker' --docker-image ruby:2.6 --description '${var.runner_name}' --tag-list 'generalpurpose' --run-untagged='true' --docker-privileged",
      "gitlab-runner start"
    ]
  }
}

#resource "null_resource" "runner-destroy" {
  #triggers = {
    #key = "key-${uuid()}"
    #private_key = var.ssh_private_key
    #host = digitalocean_droplet.runner.ipv4_address
  #}

  #provisioner "remote-exec" {
    #connection {
      #host = self.triggers.host
      #type = "ssh"
      #user = "root"
      #private_key = self.triggers.private_key
    #}
    #when    = destroy
    #inline = [
      #"gitlab-runner unregister --all-runners"
    #]
  #}

  #lifecycle {
    ## See https://github.com/hashicorp/terraform/issues/13549
    #create_before_destroy = false
  #}
#}
