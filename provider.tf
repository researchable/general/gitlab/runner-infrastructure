variable "do_token" {}
variable "ssh_private_key" {}
variable "project_registration_token" {}

# Memory for the runner nodes
variable "do_node" {
  default = "s-2vcpu-4gb"
}

variable "do_region" {
    default = "ams3"
}

# The private access token for digital ocean
provider "digitalocean" {
  token = var.do_token
}



