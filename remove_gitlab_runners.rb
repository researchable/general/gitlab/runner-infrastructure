#!/usr/bin/ruby
# frozen_string_literal: true

require 'bundler/inline'

gemfile do
  source 'https://rubygems.org'
  gem 'httparty'
  gem 'json'
end

require 'json'

GITLAB_AUTH_TOKEN = ARGV[0]
BASE = 'https://gitlab.com/api/v4'
GROUP_ID = '2928513'
HEADERS = {"PRIVATE-TOKEN": GITLAB_AUTH_TOKEN}

def make_url(path)
  BASE + path
end

def call_url(path, method, data = nil)
  url = make_url(path)
  result = HTTParty.get(url, headers: HEADERS, data: data).body if method == 'GET'
  result = HTTParty.post(url, headers: HEADERS, data: data).body if method == 'POST'
  result = HTTParty.delete(url, headers: HEADERS, data: data).body if method == 'DELETE'
  result = JSON.parse(result) if result
  result
end

runners = call_url("/groups/#{GROUP_ID}/runners", 'GET')
runner_ids = runners.map{|x| x['id']}

runner_ids.each do |runner_id|
  puts "Removing runner #{runner_id}"
  puts call_url("/runners/#{runner_id}", 'DELETE')
end
