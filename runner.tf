# When count is available, use it here https://github.com/hashicorp/terraform/issues/17519
module "runner-1" {
  source = "./modules/runner"
  do_node = var.do_node
  do_region = var.do_region
  ssh_private_key = var.ssh_private_key
  project_registration_token = var.project_registration_token
  runner_name = "runner-1"
}

module "runner-2" {
  source = "./modules/runner"
  do_node = var.do_node
  do_region = var.do_region
  ssh_private_key = var.ssh_private_key
  project_registration_token = var.project_registration_token
  runner_name = "runner-2"
}

module "runner-3" {
  source = "./modules/runner"
  do_node = var.do_node
  do_region = var.do_region
  ssh_private_key = var.ssh_private_key
  project_registration_token = var.project_registration_token
  runner_name = "runner-3"
}

module "runner-4" {
  source = "./modules/runner"
  do_node = var.do_node
  do_region = var.do_region
  ssh_private_key = var.ssh_private_key
  project_registration_token = var.project_registration_token
  runner_name = "runner-4"
}

module "runner-5" {
  source = "./modules/runner"
  do_node = var.do_node
  do_region = var.do_region
  ssh_private_key = var.ssh_private_key
  project_registration_token = var.project_registration_token
  runner_name = "runner-5"
}

module "runner-6" {
  source = "./modules/runner"
  do_node = var.do_node
  do_region = var.do_region
  ssh_private_key = var.ssh_private_key
  project_registration_token = var.project_registration_token
  runner_name = "runner-6"
}

module "runner-7" {
  source = "./modules/runner"
  do_node = var.do_node
  do_region = var.do_region
  ssh_private_key = var.ssh_private_key
  project_registration_token = var.project_registration_token
  runner_name = "runner-7"
}

module "runner-8" {
  source = "./modules/runner"
  do_node = var.do_node
  do_region = var.do_region
  ssh_private_key = var.ssh_private_key
  project_registration_token = var.project_registration_token
  runner_name = "runner-8"
}

module "runner-9" {
  source = "./modules/runner"
  do_node = var.do_node
  do_region = var.do_region
  ssh_private_key = var.ssh_private_key
  project_registration_token = var.project_registration_token
  runner_name = "runner-9"
}

module "runner-10" {
  source = "./modules/runner"
  do_node = var.do_node
  do_region = var.do_region
  ssh_private_key = var.ssh_private_key
  project_registration_token = var.project_registration_token
  runner_name = "runner-10"
}

module "runner-11" {
  source = "./modules/runner"
  do_node = var.do_node
  do_region = var.do_region
  ssh_private_key = var.ssh_private_key
  project_registration_token = var.project_registration_token
  runner_name = "runner-11"
}

module "runner-12" {
  source = "./modules/runner"
  do_node = var.do_node
  do_region = var.do_region
  ssh_private_key = var.ssh_private_key
  project_registration_token = var.project_registration_token
  runner_name = "runner-12"
}

module "runner-13" {
  source = "./modules/runner"
  do_node = var.do_node
  do_region = var.do_region
  ssh_private_key = var.ssh_private_key
  project_registration_token = var.project_registration_token
  runner_name = "runner-13"
}

module "runner-14" {
  source = "./modules/runner"
  do_node = var.do_node
  do_region = var.do_region
  ssh_private_key = var.ssh_private_key
  project_registration_token = var.project_registration_token
  runner_name = "runner-14"
}

module "runner-15" {
  source = "./modules/runner"
  do_node = var.do_node
  do_region = var.do_region
  ssh_private_key = var.ssh_private_key
  project_registration_token = var.project_registration_token
  runner_name = "runner-15"
}

module "runner-16" {
  source = "./modules/runner"
  do_node = var.do_node
  do_region = var.do_region
  ssh_private_key = var.ssh_private_key
  project_registration_token = var.project_registration_token
  runner_name = "runner-16"
}

#module "runner-17" {
#  source = "./modules/runner"
#  do_node = var.do_node
#  do_region = var.do_region
#  ssh_private_key = var.ssh_private_key
#  project_registration_token = var.project_registration_token
#  runner_name = "runner-17"
#}
#
#module "runner-18" {
#  source = "./modules/runner"
#  do_node = var.do_node
#  do_region = var.do_region
#  ssh_private_key = var.ssh_private_key
#  project_registration_token = var.project_registration_token
#  runner_name = "runner-18"
#}
#
#module "runner-19" {
#  source = "./modules/runner"
#  do_node = var.do_node
#  do_region = var.do_region
#  ssh_private_key = var.ssh_private_key
#  project_registration_token = var.project_registration_token
#  runner_name = "runner-19"
#}
#
#module "runner-20" {
#  source = "./modules/runner"
#  do_node = var.do_node
#  do_region = var.do_region
#  ssh_private_key = var.ssh_private_key
#  project_registration_token = var.project_registration_token
#  runner_name = "runner-20"
#}
#
#module "runner-21" {
#  source = "./modules/runner"
#  do_node = var.do_node
#  do_region = var.do_region
#  ssh_private_key = var.ssh_private_key
#  project_registration_token = var.project_registration_token
#  runner_name = "runner-21"
#}
#
#module "runner-22" {
#  source = "./modules/runner"
#  do_node = var.do_node
#  do_region = var.do_region
#  ssh_private_key = var.ssh_private_key
#  project_registration_token = var.project_registration_token
#  runner_name = "runner-22"
#}
#
#module "runner-23" {
#  source = "./modules/runner"
#  do_node = var.do_node
#  do_region = var.do_region
#  ssh_private_key = var.ssh_private_key
#  project_registration_token = var.project_registration_token
#  runner_name = "runner-23"
#}
#
#module "runner-24" {
#  source = "./modules/runner"
#  do_node = var.do_node
#  do_region = var.do_region
#  ssh_private_key = var.ssh_private_key
#  project_registration_token = var.project_registration_token
#  runner_name = "runner-24"
#}
