#!/usr/bin/ruby
# frozen_string_literal: true

require 'bundler/inline'

gemfile do
  source 'https://rubygems.org'
  gem 'httparty'
  gem 'json'
  gem 'byebug'
end

require 'json'
require 'byebug'

GITLAB_AUTH_TOKEN = ARGV[0]
BASE = 'https://gitlab.com/api/v4'
GROUP_ID = '2928513'
HEADERS = { "PRIVATE-TOKEN": GITLAB_AUTH_TOKEN }

def runners_from_do
  return @runners_from_do if @runners_from_do

  @runners_from_do = `doctl compute droplet list | grep 'runner-' | awk '{print $2}'`
  @runners_from_do = @runners_from_do.split("\n")
end

def make_url(path)
  BASE + path
end

def call_url(path, method, data = nil)
  url = make_url(path)
  result = HTTParty.get(url, headers: HEADERS, data: data).body if method == 'GET'
  result = HTTParty.post(url, headers: HEADERS, data: data).body if method == 'POST'
  result = HTTParty.delete(url, headers: HEADERS, data: data).body if method == 'DELETE'
  result = JSON.parse(result) if result
  result
end

runners = call_url("/groups/#{GROUP_ID}/runners", 'GET')

runners.each do |runner|
  id = runner['id']
  name = runner['description']
  next if runners_from_do.include? name

  puts "Removing runner #{id} (#{name})"
  puts call_url("/runners/#{id}", 'DELETE')
end
